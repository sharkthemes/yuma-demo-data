<?php 
$demo_lists = array(

	array(
		'title' => 'Default',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'multipurpose' ),
		'categories' => array( 'blog','multipurpose' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/default/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/default/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/default/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/default/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/default/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
		),
	),

	array(
		'title' => 'Minimal Grid',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'new', 'blog', 'portfolio', 'minimal' ),
		'categories' => array( 'new', 'blog', 'portfolio', 'minimal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/minimal-grid/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/minimal-grid/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/minimal-grid/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/minimal-grid/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/minimal-grid/',
	),

	array(
		'title' => 'Shop Two',
		'is_pro' => false,
		'type' => 'gutenberg',
		'author' => 'Shark Themes',
		'keywords' => array( 'new', 'ecommerce', 'creative' ),
		'categories' => array( 'new', 'ecommerce', 'creative' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop-two/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop-two/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop-two/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop-two/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/shop-two/',
		'plugins' => array(
			array(
				'name'      => 'WooCommerce',
				'slug'      => 'woocommerce',
			),
			array(
                'name'      => 'Ai Related Products',
                'slug'      => 'ai-related-products',
            ),
		),
	),

	array(
		'title' => 'HeadPhone',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'new', 'headphone', 'multipurpose', 'creative' ),
		'categories' => array( 'new', 'headphone','multipurpose', 'creative' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/headphone/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/headphone/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/headphone/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/headphone/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/headphone/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
            array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
		),
	),

	array(
		'title' => 'Furbish',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'new', 'furbish', 'multipurpose', 'creative' ),
		'categories' => array( 'new', 'furbish','multipurpose', 'creative' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/furbish/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/furbish/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/furbish/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/furbish/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/furbish/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Prime Slider',
                'slug'      => 'bdthemes-prime-slider-lite',
            ),
			array(
                'name'      => 'Qi Addons For Elementor',
                'slug'      => 'qi-addons-for-elementor',
            ),
            array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
		),
	),

	array(
		'title' => 'Attorney',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'new', 'law', 'corporate', 'multipurpose' ),
		'categories' => array( 'new', 'law', 'corporate', 'multipurpose' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/attorney/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/attorney/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/attorney/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/attorney/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/attorney/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
		),
	),

	array(
		'title' => 'Blog One',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal' ),
		'categories' => array( 'blog', 'portfolio', 'minimal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-one/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-one/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-one/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-one/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/blog-one/',
	),

	array(
		'title' => 'Blog Two',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'magazine' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'magazine' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-two/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-two/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-two/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-two/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/blog-two/',
	),

	array(
		'title' => 'Blog Three',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal' ),
		'categories' => array( 'blog', 'portfolio', 'minimal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-three/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-three/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-three/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-three/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/blog-three/',
	),

	array(
		'title' => 'Personal',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'personal' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'personal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/personal/',
	),

	array(
		'title' => 'Personal Dark',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'personal', 'dark' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'personal', 'dark' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-dark/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-dark/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-dark/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-dark/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/personal-dark/',
	),

	array(
		'title' => 'Masonry',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'masonry' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'masonry' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/masonry/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/masonry/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/masonry/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/masonry/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/masonry/',
	),

	array(
		'title' => 'Arabic Blog',
		'is_pro' => false,
		'pro_url' => 'https://yuma.sharkthemes.com/',
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'arabic' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'arabic' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/arabic-blog/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/arabic-blog/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/arabic-blog/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/arabic-blog/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/arabic-blog/',
	),

	array(
		'title' => 'Portfolio',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'masonry' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'masonry' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/portfolio/',
	),

	array(
		'title' => 'Blog Four',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'nature' ),
		'categories' => array( 'blog', 'portfolio', 'nature' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-four/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-four/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-four/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/blog-four/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/blog-four/',
	),

	array(
		'title' => 'Monochrome',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'monochrome' ),
		'categories' => array( 'blog', 'portfolio', 'monochrome' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/monochrome/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/monochrome/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/monochrome/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/monochrome/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/monochrome/',
	),

	array(
		'title' => 'News',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'news', 'magazine' ),
		'categories' => array( 'news','magazine' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/news/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/news/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/news/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/news/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/news/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Sina Extension For Elementor',
                'slug'      => 'sina-extension-for-elementor',
            ),
		),
	),

	array(
		'title' => 'Business',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'business', 'corporate', 'multipurpose' ),
		'categories' => array( 'business','corporate', 'multipurpose' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/business/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/business/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/business/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/business/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/business/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
			array(
                'name'      => 'Qi Addons For Elementor',
                'slug'      => 'qi-addons-for-elementor',
            ),
		),
	),

	array(
		'title' => 'Foodie',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'food', 'dark' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'food', 'dark' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/foodie/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/foodie/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/foodie/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/foodie/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/foodie/',
	),

	array(
		'title' => 'Pattern',
		'is_pro' => false,
		'type' => 'gutenberg',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'creative' ),
		'categories' => array( 'blog', 'creative' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/pattern/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/pattern/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/pattern/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/pattern/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/pattern/',
	),

	array(
		'title' => 'App',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'app', 'corporate', 'multipurpose' ),
		'categories' => array( 'app','corporate', 'multipurpose' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/app/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/app/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/app/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/app/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/app/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Qi Addons For Elementor',
                'slug'      => 'qi-addons-for-elementor',
            ),
		),
	),

	array(
		'title' => 'Portfolio Two',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'masonry' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'masonry' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio-two/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio-two/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio-two/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/portfolio-two/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/portfolio-two/',
	),

	array(
		'title' => 'Black',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'dark', 'portfolio', 'multipurpose' ),
		'categories' => array( 'blog','dark', 'portfolio', 'multipurpose' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/black/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/black/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/black/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/black/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/black/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
		),
	),

	array(
		'title' => 'Shop',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'ecommerce' ),
		'categories' => array( 'ecommerce' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/shop/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/shop/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'WooCommerce',
                'slug'      => 'woocommerce',
            ),
            array(
                'name'      => 'Prime Slider',
                'slug'      => 'bdthemes-prime-slider-lite',
            ),
			array(
                'name'      => 'Qi Addons For Elementor',
                'slug'      => 'qi-addons-for-elementor',
            ),
		),
	),

	array(
		'title' => 'Personal Blog',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'personal' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'personal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-blog/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-blog/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-blog/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-blog/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/personal-blog/',
	),

	array(
		'title' => 'Slider',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal' ),
		'categories' => array( 'blog', 'portfolio', 'minimal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/slider/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/slider/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/slider/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/slider/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/slider/',
	),

	array(
		'title' => 'Profile',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'personal', 'portfolio' ),
		'categories' => array( 'personal', 'portfolio' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/profile/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/profile/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/profile/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/profile/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/profile/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Qi Addons For Elementor',
                'slug'      => 'qi-addons-for-elementor',
            ),
            array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
		),
	),

	array(
		'title' => 'Gym',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'fitness', 'multipurpose' ),
		'categories' => array( 'fitness','multipurpose' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/gym/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/gym/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/gym/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/gym/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/gym/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Sina Extension For Elementor',
                'slug'      => 'sina-extension-for-elementor',
            ),
            array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
            array(
                'name'      => 'Timetable and Event Schedule by MotoPress',
                'slug'      => 'mp-timetable',
            ),
		),
	),

	array(
		'title' => 'Food Blog',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'food', 'minimal' ),
		'categories' => array( 'blog', 'food', 'minimal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/food-blog/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/food-blog/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/food-blog/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/food-blog/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/food-blog/',
	),

	array(
		'title' => 'Fashion',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'fashion', 'minimal' ),
		'categories' => array( 'blog', 'fashion', 'minimal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/fashion/',
	),

	array(
		'title' => 'Fashion Dark',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'fashion', 'minimal' ),
		'categories' => array( 'blog', 'fashion', 'minimal' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion-dark/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion-dark/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion-dark/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/fashion-dark/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/fashion-dark/',
	),

	array(
		'title' => 'Architecture',
		'is_pro' => false,
		'type' => 'elementor',
		'author' => 'Shark Themes',
		'keywords' => array( 'architecture', 'multipurpose', 'creative' ),
		'categories' => array( 'architecture','multipurpose', 'creative' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architecture/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architecture/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architecture/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architecture/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/architecture/',
		'plugins' => array(
			array(
				'name'      => 'Elementor',
				'slug'      => 'elementor',
			),
			array(
                'name'      => 'Prime Slider',
                'slug'      => 'bdthemes-prime-slider-lite',
            ),
			array(
                'name'      => 'Qi Addons For Elementor',
                'slug'      => 'qi-addons-for-elementor',
            ),
            array(
                'name'      => 'Contact Form 7',
                'slug'      => 'contact-form-7',
                'main_file' => 'wp-contact-form-7.php',
            ),
		),
	),

	array(
		'title' => 'Musical Blog',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'music' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'music' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/musical-blog/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/musical-blog/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/musical-blog/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/musical-blog/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/musical-blog/',
	),

	array(
		'title' => 'Architect Blog',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'portfolio', 'minimal', 'architecture' ),
		'categories' => array( 'blog', 'portfolio', 'minimal', 'architecture' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architect-blog/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architect-blog/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architect-blog/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/architect-blog/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/architect-blog/',
	),

	array(
		'title' => 'Car',
		'is_pro' => false,
		'type' => 'gutenberg',
		'author' => 'Shark Themes',
		'keywords' => array( 'dark', 'minimal', 'creative' ),
		'categories' => array( 'dark', 'minimal', 'creative' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/car/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/car/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/car/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/car/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/car/',
	),

	array(
		'title' => 'Personal Two',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'personal', 'minimal', 'creative', 'fashion' ),
		'categories' => array( 'personal', 'minimal', 'creative', 'fashion' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-two/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-two/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-two/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/personal-two/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/personal-two/',
	),

	array(
		'title' => 'Journal',
		'is_pro' => false,
		'type' => 'gutenberg',
		'author' => 'Shark Themes',
		'keywords' => array( 'blog', 'creative', 'news' ),
		'categories' => array( 'blog', 'creative', 'news' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/journal/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/journal/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/journal/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/journal/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/journal/',
	),

	array(
		'title' => 'Store',
		'is_pro' => false,
		'type' => 'gutenberg',
		'author' => 'Shark Themes',
		'keywords' => array( 'ecommerce', 'creative' ),
		'categories' => array( 'ecommerce', 'creative' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/store/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/store/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/store/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/store/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/store/',
		'plugins' => array(
			array(
				'name'      => 'WooCommerce',
				'slug'      => 'woocommerce',
			),
			array(
                'name'      => 'Ai Related Products',
                'slug'      => 'ai-related-products',
            ),
		),
	),

	array(
		'title' => 'Grid',
		'is_pro' => false,
		'type' => 'customizer',
		'author' => 'Shark Themes',
		'keywords' => array( 'personal', 'minimal', 'blog' ),
		'categories' => array( 'personal', 'minimal', 'blog' ),
		'template_url' => array(
		    'content' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/grid/content.json',
		    'options' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/grid/options.json',
		    'widgets' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/grid/widgets.json',
		),
		'screenshot_url' => 'https://gitlab.com/sharkthemes/yuma-demo-data/-/raw/master/demo/grid/screenshot.jpg',
		'demo_url' => 'https://yuma.sharkthemes.com/grid/',
	),
	
	
);